from api.repositories import *
from api import REDIS_CONN
from api.config.ingest_state import IngestStateHandler
from rq import Queue


class SQLContext:

    def __init__(self, db):
        self.db = db
        self.course_repo = course_repo.CourseRepo(self.db)
        self.course_eval_repo = course_evaluation_repo.CourseEvaluationRepo(self.db)
        self.lecture_repo = lecture_repo.LectureRepo(self.db)
        self.professor_repo = professor_repo.ProfessorRepo(self.db)
        self.professor_eval_repo = professor_evaluation_repo.ProfessorEvaluationRepo(self.db)

    def clear_data(self):
        self.lecture_repo.clear_data()
        self.professor_eval_repo.clear_data()
        self.course_eval_repo.clear_data()
        self.course_repo.clear_data()
        self.professor_repo.clear_data()


class AsyncContext:

    def __init__(self):
        self.ingest_handler = IngestStateHandler()
        self.job_queue = Queue(connection=REDIS_CONN)
