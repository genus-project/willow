from api import REDIS_CONN
from rq import Worker, Queue, Connection


listen = ['high', 'default', 'low']

if __name__ == '__main__':
    with Connection(REDIS_CONN):
        worker = Worker(list(map(Queue, listen)))
        worker.work()
