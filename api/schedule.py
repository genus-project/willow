import logging
import sys
from apscheduler.schedulers.background import BlockingScheduler
from api import async_context
from api.tasks import *

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)

logger = logging.getLogger('scheduler')

sched = BlockingScheduler()


@sched.scheduled_job('cron', day_of_week='mon-sun', hour='1')
def periodic_ingest():
    async_context.job_queue.enqueue(ingest_task.ingest)


if __name__ == '__main__':
    logger.info('Starting schedule')
    sched.start()
