from flask_sqlalchemy import BaseQuery
from sqlalchemy_searchable import SearchQueryMixin, make_searchable
from sqlalchemy_utils.types import TSVectorType
from api import db
from api.utils import to_json

make_searchable(db.metadata)


class CourseQuery(BaseQuery, SearchQueryMixin):
    pass


class Course(db.Model):
    query_class = CourseQuery

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.Unicode(15))
    term = db.Column(db.String(10))
    year = db.Column(db.String(4))
    level = db.Column(db.String(10))
    title = db.Column(db.UnicodeText)
    division = db.Column(db.String(100))

    search_vector = db.Column(TSVectorType('code', 'title'))

    @property
    def json(self):
        return to_json(self, self.__class__)
