from api import db
from api.utils import to_json


class CourseEvaluation(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dept = db.Column(db.String(10))
    code = db.Column(db.String(15))
    term = db.Column(db.String(10))
    year = db.Column(db.String(4))
    prof_id = db.Column(db.Integer, db.ForeignKey('professor.id'), nullable=False)
    instructor_enthusiasm = db.Column(db.String(5))
    workload = db.Column(db.String(5))
    recommend = db.Column(db.String(5))
    survey_invitations = db.Column(db.Integer)
    survey_completions = db.Column(db.Integer)

    @property
    def json(self):
        return to_json(self, self.__class__)

