from api import db
from api.utils import to_json


class Professor(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)

    @property
    def json(self):
        return to_json(self, self.__class__)
