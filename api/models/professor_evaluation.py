from api import db
from api.utils import to_json


class ProfessorEvaluation(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String())
    teacher_id = db.Column(db.Integer, unique=True)
    prof_id = db.Column(db.Integer, db.ForeignKey('professor.id'))
    quality = db.Column(db.String(5))
    repeat = db.Column(db.String(5))
    difficulty = db.Column(db.String(5))

    @property
    def json(self):
        return to_json(self, self.__class__)
