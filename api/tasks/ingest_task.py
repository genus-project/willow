import logging
import sys
import requests
import os
from api.models import *
from api import sql_context, async_context
from api.utils import set_none_if_na
from api import app

DATA_BASE_URL = 'https://gitlab.com/genus-project/datasets/raw/master/'
RATING_FILE = 'rating_output.json'
EVALS_FILE = 'evals_output.json'
COURSE_FILE = 'course_output.json'

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)


session = requests.session()
ingest_prefix = app.config.get('DATA_PREFIX')
is_prod = os.environ.get('APP_SETTINGS').endswith('ProductionProfile')
logger = logging.getLogger('ingest-task')


def ingest():
    logger.info('Clearing data within tables.')
    sql_context.clear_data()
    logger.info('Beginning data ingestion.')
    async_context.ingest_handler.pending()
    try:
        process_course_evaluations()
        process_course_information()
        process_rate_my_professor_ratings()
    finally:
        async_context.ingest_handler.ready()
    logger.info('Completed data ingestion.')


def process_rate_my_professor_ratings():
    url = DATA_BASE_URL + RATING_FILE
    rmp_ratings = session.get(url).json()
    professor_evals_inserted = 0

    logger.info('Beginning to ingest %s RMP reviews ....', len(rmp_ratings))
    for rating in rmp_ratings:
        prof = sql_context.professor_repo.get_by_kwargs({'name': rating.get('name')})
        if prof is not None:
            prof_id = prof.id
        else:
            prof_id = None

        prof_eval = professor_evaluation.ProfessorEvaluation(name=rating.get('name'), teacher_id=rating.get('tid'),
                                                             quality=rating.get('quality'), repeat=rating.get('repeat'),
                                                             difficulty=rating.get('difficulty'), prof_id=prof_id)
        sql_context.professor_eval_repo.create(prof_eval)
        professor_evals_inserted += 1

    logger.info('Inserted %s professor evaluations', professor_evals_inserted)
    logger.info('Completed ingesting RMP reviews')


def process_course_evaluations():
    url = DATA_BASE_URL + EVALS_FILE
    evaluations = session.get(url).json()
    logger.info('Beginning to ingest %s course evaluations ....', len(evaluations))

    course_evals_inserted, professors_inserted = 0, 0

    for evaluation in evaluations:
        if not is_prod and not evaluation.get('course_id_short').startswith(ingest_prefix):
            continue

        eval_prof = evaluation.get('professor')
        departments = evaluation.get('dept')
        instructor_enthusiasm = set_none_if_na(evaluation.get('instructor_enthusiasm'))
        workload = set_none_if_na(evaluation.get('workload'))
        recommend = set_none_if_na(evaluation.get('recommend'))
        survey_invitations = set_none_if_na(evaluation.get('survey_invitiations'))
        survey_completions = set_none_if_na(evaluation.get('survey_completions'))

        prof = sql_context.professor_repo.get_by_kwargs({'name': eval_prof})
        # Insert the professor if it is not present
        if prof is None:
            prof = sql_context.course_eval_repo.create(professor.Professor(name=eval_prof))
            professors_inserted += 1

        if len(departments) == 0:
            dept = None
        else:
            dept = departments[0]

        course_evals = course_evaluation.CourseEvaluation(code=evaluation.get('course_id_short'),
                                                          term=evaluation.get('term'), year=evaluation.get('year'),
                                                          prof_id=prof.id, dept=dept, survey_invitations=survey_invitations,
                                                          survey_completions=survey_completions, instructor_enthusiasm=instructor_enthusiasm,
                                                          workload=workload, recommend=recommend)
        sql_context.course_eval_repo.create(course_evals)
        course_evals_inserted += 1

    logger.info('Inserted %s course evaluations, %s professors', course_evals_inserted, professors_inserted)
    logger.info('Completed ingesting course evaluations')


def process_course_information():
    url = DATA_BASE_URL + COURSE_FILE
    courses_info = session.get(url).json()
    logger.info('Beginning to ingest %s courses ....', len(courses_info))

    courses_inserted, lectures_inserted, professors_inserted = 0, 0, 0

    for course_info in courses_info:
        if not is_prod and not course_info.get('course_id_short').startswith(ingest_prefix):
            continue

        term = course_info.get('term').split()
        year = term[0]
        term = " ".join(term[1:])
        course_obj = sql_context.course_repo.create(course.Course(code=course_info.get('course_id_short'),
                          term=term, year=year, level=course_info.get('level'),
                          division=course_info.get('division'), title=course_info.get('title')))
        courses_inserted += 1

        activities = course_info.get('sections')
        for lecture_activity in activities.get('lecture'):
            professor_list = lecture_activity.get('professor')

            if len(professor_list) == 0:
                prof_id = None
            else:
                # Use first professor for simplicity, event though some lectures have multiple professors for a
                # single section
                prof_name = professor_list[0]
                prof = sql_context.professor_repo.get_by_kwargs({'name': prof_name})
                # Insert that professor
                if prof is None:
                    prof = sql_context.course_eval_repo.create(professor.Professor(name=prof_name))
                    professors_inserted += 1
                prof_id = prof.id

            lecture_obj = lecture.Lecture(course_id=course_obj.id, activity=lecture_activity.get('activity'), time=lecture_activity.get('time'),
                            size=lecture_activity.get('size'), delivery=lecture_activity.get('delivery'), prof_id=prof_id)
            sql_context.lecture_repo.create(lecture_obj)
            lectures_inserted += 1

    logger.info('Inserted %s courses, %s lectures, %s professors', courses_inserted, lectures_inserted, professors_inserted)
    logger.info('Completed ingesting courses')
