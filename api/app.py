import logging
import sys
import os
from flask import jsonify, request
from flasgger import Swagger, swag_from
from collections import defaultdict
from api import app, async_context, sql_context, swagger_template, swagger_config
from api.utils import get_dept_from_code
from api.tasks.ingest_task import ingest

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)

logger = logging.getLogger('flask-api')

swag = Swagger(
    app=app, config=swagger_config, template=swagger_template)


@app.errorhandler(404)
def not_found_error(error):
    return jsonify({'message': 'Page not found.'}), 404


@swag_from('/api/docs/ping.yml')
@app.route('/ping')
def ping():
    return jsonify({'message': 'pong'})


@swag_from('/api/docs/course_analysis.yml')
@app.route('/v1/courses/analyze/<code>', methods=['GET'])
def get_course_analyze(code):
    """
    Analyze a course by aggregating its historic course evaluation data and calclulating values
    for professors teaching this course, the department averages, and more.
    :param code:
    :return: dict
    """
    v = sql_context.course_repo.__class__
    course_query_result = sql_context.course_repo.__class__.get_by_kwargs({'code': code})
    if course_query_result is None:
        return jsonify({'message': 'Course not found.'}), 404

    course_result = course_query_result.json
    course_result.pop('term', None)
    course_result.pop('year', None)

    # Here we are getting all courses that match the specified code
    all_courses_with_code_query_result = sql_context.course_repo.__class__.get_all_by_kwargs({'code': code})
    offerings_result, professor_id_set = sql_context.lecture_repo.__class__.get_lectures_for_course(all_courses_with_code_query_result)

    historical_averages_for_course_result = sql_context.course_eval_repo.__class__.\
        get_historical_averages_by_kwargs({'code': code})

    historical_averages_for_dept_result = sql_context.course_eval_repo.__class__. \
        get_historical_averages_by_kwargs({'dept': get_dept_from_code(code)})

    historical_averages_for_professors = defaultdict(dict)
    for prof_id in professor_id_set:
        historical_averages_for_prof_id = sql_context.course_eval_repo.__class__. \
            get_historical_averages_by_kwargs({'prof_id': prof_id})

        prof = sql_context.professor_repo.__class__.get_by_kwargs({'id': prof_id})
        historical_averages_for_professors[prof.name] = historical_averages_for_prof_id

    result, analysis = dict(), dict()
    result['information'] = course_result
    result['offerings'] = offerings_result
    analysis['overall_course_analysis'] = historical_averages_for_course_result
    analysis['professors_analysis'] = historical_averages_for_professors
    analysis['dept_analysis'] = historical_averages_for_dept_result
    result['analysis'] = analysis

    return jsonify({'data': result})


@swag_from('/api/docs/course_search.yml')
@app.route('/v1/courses/search', methods=['GET'])
def get_course_search_results():
    """
    Search all the course codes and descriptions given a keyword.
    :return: List of Courses that match query.
    """
    query = request.args.get('keyword')
    if query is None or not query.strip():
        return jsonify({'message': 'Missing query string or query string is string is empty'}), 400

    search_results = sql_context.course_repo.search(query.strip())
    return jsonify({'data': search_results})


if __name__ == '__main__':
    profile = app.config.get('APP_SETTINGS')
    if profile is not None and profile.endswith('ProductionProfile'):
        async_context.job_queue.enqueue(ingest, job_timeout=app.config.get('MAX_TIMEOUT'))
    app.run(debug=True, use_reloader=False, host='0.0.0.0', port=app.config.get('SERVER_PORT'))
