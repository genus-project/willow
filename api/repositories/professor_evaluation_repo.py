from api.repositories.base import Base
from api.models.professor_evaluation import ProfessorEvaluation


class ProfessorEvaluationRepo(Base):

    def clear_data(self):
        ProfessorEvaluation.query.delete()
        self.db.session.commit()

    @staticmethod
    def get_by_kwargs(kwarg_dict) -> ProfessorEvaluation:
        prof_review = ProfessorEvaluation.query.filter_by(**kwarg_dict).first()
        return prof_review

    @staticmethod
    def get_all_by_kwargs(kwarg_dict) -> list:
        all_prof_reviews = ProfessorEvaluation.query.filter_by(**kwarg_dict).all()
        if all_prof_reviews is None:
            return []
        return all_prof_reviews