from api.repositories.base import Base
from api.models.course_evaluation import CourseEvaluation
from api.utils import get_decimal
from api.utils import calculate_average
from collections import defaultdict


class CourseEvaluationRepo(Base):

    def clear_data(self):
        CourseEvaluation.query.delete()
        self.db.session.commit()

    @staticmethod
    def get_by_kwargs(kwarg_dict) -> CourseEvaluation:
        course_eval = CourseEvaluation.query.filter_by(**kwarg_dict).first()
        return course_eval

    @staticmethod
    def get_all_by_kwargs(kwarg_dict) -> list:
        all_course_evals = CourseEvaluation.query.filter_by(**kwarg_dict).all()
        if all_course_evals is None:
            return []
        return all_course_evals

    @staticmethod
    def get_historical_averages_by_kwargs(kwarg_dict) -> dict:
        results = CourseEvaluation.query.filter_by(**kwarg_dict).filter(CourseEvaluation.year >= '2018').all()
        if results is None:
            return {}

        recommend_dict, instructor_enthusiasm_dict, workload_dict = defaultdict(list), defaultdict(list), defaultdict(list)

        for evaluation in results:
            survey_completions = get_decimal(evaluation.survey_completions)
            # Special case to account for summer terms.
            term = evaluation.term.split()[0]
            recommend_dict[term].append((get_decimal(evaluation.recommend), survey_completions))
            workload_dict[term].append((get_decimal(evaluation.workload), survey_completions))
            instructor_enthusiasm_dict[term].append((get_decimal(evaluation.instructor_enthusiasm), survey_completions))

        historical_averages = defaultdict(dict)

        recommend_total_average_sum, recommend_total_weight = 0, 0
        for key, value in recommend_dict.items():
            average, weight = calculate_average(value)
            recommend_total_weight += weight
            recommend_total_average_sum += weight * (average/5)

            recommend_averages = {'average': int(average*20), 'weight': int(weight)}
            historical_averages[key]['recommend'] = recommend_averages

        if recommend_total_weight > 0:
            recommend_total = {'average': int(recommend_total_average_sum * 100 / recommend_total_weight),
                               'weight': int(recommend_total_weight)}
        else:
            recommend_total = {'average': int(recommend_total_average_sum * 100 / 1),
                               'weight': int(recommend_total_weight)}
        historical_averages['total']['recommend'] = recommend_total

        instructor_enthusiasm_total_average_sum, instructor_enthusiasm_total_weight = 0, 0
        for key, value in instructor_enthusiasm_dict.items():
            average, weight = calculate_average(value)
            instructor_enthusiasm_total_weight += weight
            instructor_enthusiasm_total_average_sum += weight * (average/5)

            instructor_enthusiasm_averages = {'average': int(average*20), 'weight': int(weight)}
            historical_averages[key]['instructor_enthusiasm'] = instructor_enthusiasm_averages

        if instructor_enthusiasm_total_weight > 0:
            instructor_enthusiasm_total = {'average': int(instructor_enthusiasm_total_average_sum * 100 / instructor_enthusiasm_total_weight),
                                           'weight': int(instructor_enthusiasm_total_weight)}
        else:
            instructor_enthusiasm_total = {'average': int(instructor_enthusiasm_total_average_sum * 100 / 1),
                                           'weight': int(instructor_enthusiasm_total_weight)}
        historical_averages['total']['instructor_enthusiasm'] = instructor_enthusiasm_total

        workload_total_average_sum, workload_total_weight = 0, 0
        for key, value in workload_dict.items():
            average, weight = calculate_average(value)
            workload_total_weight += weight
            workload_total_average_sum += weight * (average/5)

            workload_averages = {'average': int(average*20), 'weight': int(weight)}
            historical_averages[key]['workload'] = workload_averages

        if workload_total_weight > 0:
            workload_total = {'average': int(workload_total_average_sum * 100 / workload_total_weight),
                                           'weight': int(workload_total_weight)}
        else:
            workload_total = {'average': int(workload_total_average_sum * 100 / 1),
                                           'weight': int(workload_total_weight)}
        historical_averages['total']['workload'] = workload_total

        return dict(historical_averages)
