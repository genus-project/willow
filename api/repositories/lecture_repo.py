from api.repositories.base import Base
from api.models.lecture import Lecture
from api.repositories.professor_repo import ProfessorRepo
from collections import defaultdict


class LectureRepo(Base):

    def clear_data(self):
        Lecture.query.delete()
        self.db.session.commit()

    @staticmethod
    def get_by_kwargs(kwarg_dict) -> Lecture:
        lecture = Lecture.query.filter_by(**kwarg_dict).first()
        return lecture

    @staticmethod
    def get_all_by_kwargs(kwarg_dict) -> list:
        all_lectures = Lecture.query.filter_by(**kwarg_dict).all()
        if all_lectures is None:
            return []
        return all_lectures

    @staticmethod
    def get_lectures_for_course(courses_list):
        offerings = defaultdict(dict)
        lecture_professors = set()
        for course in courses_list:
            year_term = '{} {}'.format(course.year, course.term)
            lecture_result = Lecture.query.filter_by(course_id=course.id).first()

            if lecture_result:
                offering = lecture_result.json

                if lecture_result.prof_id:
                    prof = ProfessorRepo.get_by_kwargs({'id' : lecture_result.prof_id})
                    offering['prof_name'] = prof.name
                    lecture_professors.add(lecture_result.prof_id)

                offerings[year_term]['lecture'] = offering
        return dict(offerings), lecture_professors
