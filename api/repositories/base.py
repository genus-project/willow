class Base(object):
    def __init__(self, db):
        self.db = db

    def session(self):
        return self.db.session

    def create(self, item):
        self.db.session.add(item)
        self.db.session.commit()
        return item