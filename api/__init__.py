import os
import redis
import yaml
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])

db = SQLAlchemy(app)

REDIS_URL = app.config.get('REDIS_URL')
REDIS_CONN = redis.from_url(REDIS_URL)

# Fix to resolve circular imports, redesign in progress
from api.config.context import *

sql_context = SQLContext(db)
async_context = AsyncContext()

with open('api/docs/template.yml', 'r') as stream:
    swagger_template = yaml.load(stream, yaml.SafeLoader)

swagger_template['host'] = app.config.get('HOST')

if app.config.get('DISABLE_SWAGGER'):
    swagger_config = {
        'headers': [
        ],
        'specs': [
            {
                'endpoint': 'apispec',
                'route': '/apispec.json',
                'rule_filter': lambda rule: True,
                'model_filter': lambda tag: True,
            }
        ],
        'swagger_ui': False
    }
else:
    swagger_config = None