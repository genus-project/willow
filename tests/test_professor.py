from unittest import mock
from api.models import professor
from api.repositories import professor_repo


@mock.patch('api.repositories.professor_repo.Professor')
def test_professor_get_by_kwargs_when_professor_matches(mockProfessor):
    test_prof = professor.Professor(name='James')
    filter_by_mock = mockProfessor.query.filter_by.return_value
    filter_by_mock.all.return_value = [test_prof]

    lectures = professor_repo.ProfessorRepo.get_all_by_kwargs({'name': 'James'})
    assert lectures == [test_prof]


@mock.patch('api.repositories.professor_repo.Professor')
def test_professor_get_by_kwargs_when_no_professor_matches(mockProfessor):
    filter_by_mock = mockProfessor.query.filter_by.return_value
    filter_by_mock.all.return_value = None

    lectures = professor_repo.ProfessorRepo.get_all_by_kwargs({'name': 'James'})
    assert lectures == []
