import json
from unittest import mock
from api.repositories import *

COURSE_SEARCH_ENDPOINT = '/v1/courses/search'
COURSE_ANALYSIS_ENDPOINT = '/v1/courses/analyze/{}'


def test_course_search_when_keyword_is_missing(app):
    client = app.test_client()
    resp = client.get(COURSE_SEARCH_ENDPOINT)
    data = json.loads(resp.data)

    assert resp.status_code == 400
    assert data.get('message') == 'Missing query string or query string is string is empty'


def test_course_search_when_keyword_is_whitespace(app):
    client = app.test_client()
    resp = client.get(COURSE_SEARCH_ENDPOINT, query_string={'keyword': '     '})
    data = json.loads(resp.data)

    assert resp.status_code == 400
    assert data.get('message') == 'Missing query string or query string is string is empty'


@mock.patch('api.app.sql_context')
def test_course_search_when_keyword_matches_courses(mock_sql_context, app):
    test_courses = [('CSC108H1', '', '', ''), ('CSC148H1', '', '', ''), ('CSC165H1', '', '', '')]
    mock_sql_context.course_repo.search.return_value = test_courses

    client = app.test_client()
    resp = client.get(COURSE_SEARCH_ENDPOINT, query_string={'keyword': 'CSC'})
    data = json.loads(resp.data)

    assert resp.status_code == 200
    assert data.get('data') == [list(course) for course in test_courses]


@mock.patch('api.app.sql_context')
def test_course_search_when_key_does_not_match_courses(mock_sql_context, app):
    mock_sql_context.course_repo.search.return_value = []

    client = app.test_client()
    resp = client.get(COURSE_SEARCH_ENDPOINT, query_string={'keyword': 'CSC'})
    data = json.loads(resp.data)

    assert resp.status_code == 200
    assert data.get('data') == []


@mock.patch('api.app.sql_context')
def test_course_analysis_when_key_does_not_match_courses(mock_sql_context, app):
    pass


