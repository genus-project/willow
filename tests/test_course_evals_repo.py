from unittest import mock
from api.models import course_evaluation
from api.repositories import course_evaluation_repo


@mock.patch('api.repositories.course_evaluation_repo.CourseEvaluation')
def test_course_evaluation_get_by_kwargs_when_course_evaluation_matches(mockCourseEvaluation):
    test_course_eval = course_evaluation.CourseEvaluation(year='2019')
    filter_by_mock = mockCourseEvaluation.query.filter_by.return_value
    filter_by_mock.all.return_value = [test_course_eval]

    course_evals = course_evaluation_repo.CourseEvaluationRepo.get_all_by_kwargs({'year': '2019'})
    assert course_evals == [test_course_eval]


@mock.patch('api.repositories.course_evaluation_repo.CourseEvaluation')
def test_course_evaluation_get_by_kwargs_when_no_course_evaluation_matches(mockCourseEvaluation):
    filter_by_mock = mockCourseEvaluation.query.filter_by.return_value
    filter_by_mock.all.return_value = None

    course_evals = course_evaluation_repo.CourseEvaluationRepo.get_all_by_kwargs({'year': '2019'})
    assert course_evals == []


@mock.patch('api.repositories.course_evaluation_repo.CourseEvaluation')
def test_course_evaluation_get_all_by_kwargs_when_course_evaluation_matches(mockCourseEvaluation):
    mock_course_eval = course_evaluation.CourseEvaluation(year=2019)
    filter_by_mock = mockCourseEvaluation.query.filter_by.return_value
    filter_by_mock.all.return_value = [mock_course_eval]

    course_evals = course_evaluation_repo.CourseEvaluationRepo.get_all_by_kwargs({'year': '2019'})
    assert course_evals == [mock_course_eval]


@mock.patch('api.repositories.course_evaluation_repo.CourseEvaluation')
def test_course_evaluation_get_historical_averages_by_kwargs_when_no_course_evaluation_matches(mockCourseEvaluation):
    mockCourseEvaluation.year = '2015'
    filter_mock = mockCourseEvaluation.query.filter_by.return_value.filter.return_value
    filter_mock.all.return_value = None

    course_evals = course_evaluation_repo.CourseEvaluationRepo.get_historical_averages_by_kwargs({'code': 'CSC108H1'})
    assert course_evals == {}


@mock.patch('api.repositories.course_evaluation_repo.CourseEvaluation')
def test_course_evaluation_get_historical_averages_by_kwargs_when_course_evaluation_matches_but_survey_is_none(mockCourseEvaluation):
    mock_course_eval_1 = course_evaluation.CourseEvaluation(dept='CSC', code='CSC108H1', term='2019 Fall', prof_id=1,
                                                            instructor_enthusiasm=3, workload=3, recommend=3)
    mockCourseEvaluation.year = '2015'
    filter_mock = mockCourseEvaluation.query.filter_by.return_value.filter.return_value
    filter_mock.all.return_value = [mock_course_eval_1]

    course_evals = course_evaluation_repo.CourseEvaluationRepo.get_historical_averages_by_kwargs({'code': 'CSC108H1'})
    average = {'average': 0, 'weight': 0}
    assert course_evals == {'2019': {'recommend': average, 'instructor_enthusiasm': average, 'workload': average},
                            'total': {'recommend': average, 'instructor_enthusiasm': average, 'workload': average}}


@mock.patch('api.repositories.course_evaluation_repo.CourseEvaluation')
def test_course_evaluation_get_historical_averages_by_kwargs_when_course_evaluation_matches(mockCourseEvaluation):
    mock_course_eval_1 = course_evaluation.CourseEvaluation(dept='CSC', code='CSC108H1', term='2019 Fall', prof_id=1,
                                                            instructor_enthusiasm=3, workload=3, recommend=3,
                                                            survey_completions=5, survey_invitations=5)
    mock_course_eval_2 = course_evaluation.CourseEvaluation(dept='CSC', code='CSC108H1', term='2018 Winter', prof_id=1,
                                                            instructor_enthusiasm=5, workload=5, recommend=5,
                                                            survey_completions=5, survey_invitations=5)
    mockCourseEvaluation.year = '2015'
    filter_mock = mockCourseEvaluation.query.filter_by.return_value.filter.return_value
    filter_mock.all.return_value = [mock_course_eval_1, mock_course_eval_2]

    course_evals = course_evaluation_repo.CourseEvaluationRepo.get_historical_averages_by_kwargs({'code': 'CSC108H1'})
    average_2018 = {'recommend': {'average': 100, 'weight': 5}, 'instructor_enthusiasm': {'average': 100, 'weight': 5}, 'workload': {'average': 100, 'weight': 5}}
    average_2019 = {'recommend': {'average': 60, 'weight': 5}, 'instructor_enthusiasm': {'average': 60, 'weight': 5},'workload': {'average': 60, 'weight': 5}}
    average_total = {'recommend': {'average': 80, 'weight': 10}, 'instructor_enthusiasm': {'average': 80, 'weight': 10}, 'workload': {'average': 80, 'weight': 10}}
    assert course_evals == {'2019': average_2019, '2018': average_2018, 'total': average_total}

